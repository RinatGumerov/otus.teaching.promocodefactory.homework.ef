﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomersPepository : IRepository<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomersByPreferenceIdAsync(Guid preferenceId);

        Task<Customer> AddNewPromocodeAsync(Guid customerId, PromoCode promoCode);

    }
}
