﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Getting all entities
        /// </summary>
        /// <returns>Entity collection</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Entity</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Get entitys range by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Entyty collection</returns>
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);    

        /// <summary>
        /// Add entity
        /// </summary>
        /// <param name="item">Entity</param>
        Task AddAsync(T item);

        /// <summary>
        /// Add entity range
        /// </summary>
        /// <param name="items">Entitys collection</param>
        /// <returns>Add result</returns>
        Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> items);

        /// <summary>
        /// Remove entity by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Removing result </returns>
        Task<bool> RemoveAsync(Guid id);

        /// <summary>
        /// Return exists of item by id
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>Item exists</returns>
        Task<bool> ExistsAsync(Guid id);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        Task UpdateAsync(T entity);
    }
}