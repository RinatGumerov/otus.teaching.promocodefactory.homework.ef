﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Promocodes
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferencesRepository;
        private readonly ICustomersPepository _customerRepository;
        private readonly IPromoCodesRepository _promoCodesRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromocodesController(
            IRepository<Preference> preferencesRepository,
            ICustomersPepository customersRepository,
            IPromoCodesRepository promoCodesRepository,
            IRepository<Employee> employeeRepository
            )
        {
            _preferencesRepository = preferencesRepository;
            _customerRepository = customersRepository;
            _promoCodesRepository = promoCodesRepository;
            _employeeRepository = employeeRepository;
        }


        /// <summary>
        /// Get All Promocodes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();
            var employeesModelList = promocodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                BeginDate = x.BeginDate.ToString(),
                Code = x.Code,
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
            }).ToList();
            return employeesModelList;

        }

        /// <summary>
        /// Create promocode and add to customers
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promocodes = new List<PromoCode>();
            Guid preferenceId = Guid.Parse(request.Preference);
            var employee = (await _employeeRepository.GetAllAsync()).First();
            foreach (var customer in await _customerRepository.GetCustomersByPreferenceIdAsync(preferenceId))
            {
                var newPromo = new PromoCode()
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    PreferenceId = preferenceId,
                    CustomerId = customer.Id,
                    PartnerManagerId = employee.Id
                };

                promocodes.Add(newPromo);
            }

            return (await _promoCodesRepository.AddRangeAsync(promocodes)).Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    BeginDate = x.BeginDate.ToString(),
                    Code = x.Code,
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName
                }
            ).ToList();
        }
    }
}