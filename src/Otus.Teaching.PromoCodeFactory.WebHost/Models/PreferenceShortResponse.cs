﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceShortResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }


        public PreferenceShortResponse()
        {
        }

        public PreferenceShortResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }
    }


}
