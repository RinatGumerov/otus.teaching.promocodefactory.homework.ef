﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class EmployeesConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(t => t.FirstName).HasMaxLength(50);
            builder.Property(t => t.LastName).HasMaxLength(50);
            builder.Property(t => t.Email).HasMaxLength(50);
            builder.HasOne(t => t.Role);
        }
    }

    public class RolesConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(50);
            builder.Property(t => t.Description).HasMaxLength(200);
        }
    }

    public class CustomersConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(t => t.FirstName).HasMaxLength(50);
            builder.Property(t => t.LastName).HasMaxLength(50);
            builder.Property(t => t.Email).HasMaxLength(50);
            builder.HasMany(t => t.CustomerPreferences);
            builder.HasMany(t => t.PromoСodes);
        }
    }

    public class PreferencesConfiguration : IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(50);
        }
    }

    public class PromoCodesConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(t => t.Code).HasMaxLength(20);
            builder.Property(t => t.ServiceInfo).HasMaxLength(150);
            builder.Property(t => t.PartnerName).HasMaxLength(50);
            builder.HasOne(t => t.PartnerManager);
            builder.HasOne(t => t.Preference);
        }
    }

    public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder.HasKey(t => new { t.CustomerId, t.PreferenceId });

            builder.HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            builder.HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp => cp.PreferenceId);
        }
    }

}
