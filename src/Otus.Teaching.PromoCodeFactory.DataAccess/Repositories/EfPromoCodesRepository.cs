﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPromoCodesRepository : EfRepository<PromoCode>, IPromoCodesRepository
    {
        public EfPromoCodesRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public override async Task<PromoCode> AddAsync(PromoCode promoCode)
        {
            _dataContext.Customers.Attach(promoCode.Customer);
            await _dataContext.AddAsync(promoCode);
            await _dataContext.SaveChangesAsync();

            return promoCode;
        }
    }

}
