﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Task.FromResult(Data);
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task AddAsync(T entity)
        {
            await Task.Run(() => Data.Add(entity));
        }

        public Task<bool> RemoveAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            return Task.FromResult(item is { } && Data.Remove(item));
        }

        public Task<bool> ExistsAsync(Guid id)
        {
            var exists = Data.Any(x => x.Id == id);
            return Task.FromResult(exists);
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> items)
        {
            throw new NotImplementedException();
        }
    }
}